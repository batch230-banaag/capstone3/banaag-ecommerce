const Product = require("../models/Products");
const auth = require("../auth");
const products = require("../models/Products");
const { result } = require("lodash");

module.exports.addProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    stocks: req.body.stocks,
    imgSrc: req.body.imgSrc,
  });

  if (userData.isAdmin) {
    return await newProduct
      .save()
      .then((product) => {
   
        res.send(true);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  } else {
    return res.status(401).send(false);
  }
};

module.exports.allProducts = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    return await Product.find({})
      .then((products) => {
        res.send(products);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  } else {
    return await Product.find({ isActive: true })
      .then((products) => {

        res.send(products);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  }
};

module.exports.actveProducts = async (req, res) => {
  return await Product.find({ isActive: true })
    .then((products) => {
      res.send(products);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.specificProduct = async (req, res) => {
  return await Product.findById(req.params.prodId)
    .then((products) => {

      res.send(products);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.search = async (req, res) => {
  const searchTerm = req.params.item;
  return await Product.find({ name: { $regex: searchTerm, $options: "i" } })
    .then((products) => {

      res.send(products);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.updateProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    let updatedProduct = {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
    };
    return await Product.findByIdAndUpdate(req.params.prodId, updatedProduct, {
      new: true,
    })
      .then((result) => {
        res.send(true);
      })
      .catch((error) => {
        console.log(error);
        res.send(false);
      });
  } else {
    return res.status(401).send(false);
  }
};

module.exports.archiveProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    return await Product.findByIdAndUpdate(
      req.params.prodId,
      {
        isActive: req.body.isActive,
      },
      {
        new: true,
      }
    )
      .then((result) => {

        res.send(true);
      })
      .catch((error) => {
        console.log(error);
        res.send(false);
      });
  } else {
    return res.status(401).send(false);
  }
};

module.exports.addStocks = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  const stocksToAdd = req.body.stocks;

  if (userData.isAdmin == true) {
    let isStocksUpdated = Product.findByIdAndUpdate(req.params.prodId).then(
      (product) => {
        product.stocks += stocksToAdd;

        return product.save().catch((error) => {
          console.log(error);
          return false;
        });
      }
    );
  } else {
    return res.status(401).send(false);
  }
};
