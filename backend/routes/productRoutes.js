const express = require("express");
const router = express.Router();
const productsController = require("../controllers/productsController");
const auth = require("../auth");

router.post("/addProduct", auth.verify, productsController.addProduct);

router.get("/allProducts", auth.verify, productsController.allProducts);

router.get("/actveProducts", productsController.actveProducts);

router.get("/search/:item", productsController.search);

router.get("/:prodId", productsController.specificProduct);

router.patch("/:prodId/update", auth.verify, productsController.updateProduct);

router.patch(
  "/:prodId/archive",
  auth.verify,
  productsController.archiveProduct
);

router.patch("/:prodId/addStocks", auth.verify, productsController.addStocks);

module.exports = router;
