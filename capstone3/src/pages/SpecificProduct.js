import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { InputLabel, MenuItem, Select, FormControl } from "@mui/material/";
import { Snackbar, IconButton, CloseIcon } from "@mui/material";
import axios from "axios";

function SpecificProduct() {
  const { prodId } = useParams();
  const [productData, setProductData] = useState([]);
  const [quantity, setQuantity] = useState(1);
  const navigate = useNavigate();

  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const token = localStorage.getItem("token");

  function addToCart() {
    fetch(`${process.env.REACT_APP_API_URL}/users/addCart`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productId: productData._id,
        productName: productData.name,
        quantity: quantity,
        price: productData.price,
        imgSrc: productData.imgSrc,
      }),
    }).then((res) => {
      return res.json();
    });
  }

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_API_URL}/products/${prodId}`)
      .then((res) => {
        // console.log(res.data);
        return setProductData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [prodId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col xs={12} lg={6}>
          <img src={productData.imgSrc} alt="ring" className="img-fluid" />
        </Col>
        <Col style={{ padding: "50px" }}>
          <Row
            style={{
              borderBottom: "5px solid black",
            }}
          >
            <h2>{productData.name}</h2>
          </Row>
          <Row style={{ height: "10vh" }}>
            <h3 style={{ fontWeight: "bold" }}>₱{productData.price}</h3>
          </Row>

          <Row className="mb-3" style={{ width: "40%" }}>
            <FormControl>
              <InputLabel id="demo-simple-select-label">Quantity</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                placeholder="1"
                id="demo-simple-select"
                value={quantity}
                label="Quantity"
                onChange={(e) => {
                  setQuantity(e.target.value);
                }}
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
              </Select>
            </FormControl>
          </Row>

          <Row>
            <p>Remaining stocks: {productData.stocks}</p>
          </Row>

          <Row>
            <Button
              variant="dark"
              className="mb-3"
              onClick={() => {
                handleClick();
                addToCart();
              }}
            >
              Add to Cart
            </Button>

            <Snackbar
              open={open}
              autoHideDuration={6000}
              onClose={handleClose}
              severity="success"
              message="Item successfully added to Cart!"
            />
          </Row>
          <Row>
            <Button
              variant="light"
              className="mb-5"
              style={{ border: "2px solid black" }}
              onClick={() => {
                addToCart();
                navigate("/checkout");
              }}
            >
              Buy now
            </Button>
          </Row>

          <Row>
            <p>
              This item is available for pick-up. Place your order online and
              pick it up in person at select boutiques in 2 - 5 business days
            </p>
          </Row>
        </Col>
      </Row>
      <Row className="p-3">
        <Col lg={8} className="mb-4">
          <h4>Product description</h4>
          <p className="text-justified">{productData.description}</p>
        </Col>
      </Row>
      <Row></Row>
    </Container>
  );
}

export default SpecificProduct;
