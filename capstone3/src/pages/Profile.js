import { useState, useEffect } from "react";
import { Container, Row, Col, Table, Accordion } from "react-bootstrap";

function Profile() {
  const token = localStorage.getItem("token");

  const [userDetails, setUserDetails] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((user) => {
        setUserDetails(
          <Container>
            <Row>
              <h2>
                {user.firstName} {user.lastName}
              </h2>
            </Row>
            <Row>
              <h5>Email: {user.email}</h5>
            </Row>
            <Row>
              <h5>Mobile Number: {user.mobileNumber}</h5>
            </Row>
            <Row className="mt-5">
              <h3>Transactions</h3>
            </Row>
            <Accordion
              defaultActiveKey={["0"]}
              alwaysOpen
              style={{ height: "100%", marginBottom: 50 }}
            >
              {user.orders.map((order) => (
                <Accordion.Item eventKey={order._id}>
                  <Accordion.Header>
                    Order ID: {order._id} Purchase Date: {order.purchasedOn}
                  </Accordion.Header>
                  <Accordion.Body>
                    <Row style={{ fontWeight: "bold" }}>
                      <Col>Product Name</Col>
                      <Col>Quantity</Col>
                    </Row>
                    {order.products.map((product) => (
                      <Row key={product._id}>
                        <Col>{product.productName}</Col>
                        <Col>{product.quantity}</Col>
                      </Row>
                    ))}
                    <Row className="text-right mt-4">
                      <h5>Total: ₱ {order.totalAmount}</h5>
                    </Row>
                  </Accordion.Body>
                </Accordion.Item>
              ))}
            </Accordion>
          </Container>
        );
      });
  }, [token]);

  return (
    <Container
      className="p-3 mt-3"
      style={{ height: "100%", marginBottom: "100px" }}
    >
      {userDetails}
    </Container>
  );
}

export default Profile;
