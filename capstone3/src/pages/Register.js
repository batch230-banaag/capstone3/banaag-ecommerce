import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  const navigate = useNavigate();
  const user = localStorage.getItem("email");

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((result) => {
        if (result === true) {
          setFirstName("");
          setLastName("");
          setEmail("");
          setMobileNumber("");
          setPassword1("");
          setPassword2("");

          Swal.fire({
            title: "Email already exists",
            icon: "error",
            text: "Please try another email.",
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: { "Content-type": "application/json" },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1,
              mobileNumber: mobileNumber,
            }),
          })
            .then((res) => {
              return res.json();
            })

            .then((result) => {
              console.log(result);
              if (result === true) {
                Swal.fire({
                  title: "Successfully registered",
                  icon: "success",
                  text: "You have successfully registered.",
                });
                navigate("/login");
              } else {
                Swal.fire({
                  title: "Something went wrong",
                  icon: "error",
                  text: "Please try again.",
                });
              }
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  //   axios
  //     .post("http://localhost:4000/users/checkEmail", { email: email })
  //     .then((result) => {
  //       if (result.data === true) {
  //         setFirstName("");
  //         setLastName("");
  //         setEmail("");
  //         setMobileNumber("");
  //         setPassword1("");
  //         setPassword2("");

  //         Swal.fire({
  //           title: "Email already exists",
  //           icon: "error",
  //           text: "Please try another email.",
  //         });
  //       } else {
  //         axios
  //           .post("http://localhost:4000/users/register", {
  //             firstName: firstName,
  //             lastName: lastName,
  //             email: email,
  //             password: password1,
  //             mobileNumber: mobileNumber,
  //           })
  //           .then((result) => {
  //             if (result.data === true) {
  //               Swal.fire({
  //                 title: "Successfully registered",
  //                 icon: "success",
  //                 text: "You have successfully registered.",
  //               });
  //               navigate("/login");
  //             } else {
  //               Swal.fire({
  //                 title: "Something went wrong",
  //                 icon: "error",
  //                 text: "Please try again.",
  //               });
  //             }
  //           })
  //           .catch((err) => {
  //             console.log(err);
  //           });
  //       }
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNumber.length === 11 &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNumber, password1, password2]);

  return user !== null ? (
    <Navigate to="/" />
  ) : (
    <Container
      style={{ height: "100%" }}
      className="d-flex justify-content-center align-items-center"
    >
      <Col id="login" className="p-5 mt-5 mb-5" lg={5}>
        <Form onSubmit={(event) => registerUser(event)}>
          <h1 className="text-center mb-4">Register</h1>
          <Form.Group controlId="useFirstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter first name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group controlId="useLastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter last name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="userNumber">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter mobile number"
              value={mobileNumber}
              onChange={(e) => setMobileNumber(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Verify Password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3 mt-5" controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Agree to Terms & Conditions" />
          </Form.Group>
          <Form.Group className="mb-5" controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Subscribe to Newsletter" />
          </Form.Group>
          <Row>
            {isActive ? (
              <Button
                variant="dark"
                type="submit"
                id="submitBtn"
                className="mt-3"
                onClick={registerUser}
              >
                Register
              </Button>
            ) : (
              <Button
                variant="dark"
                type="submit"
                id="submitBtn"
                className="mt-3"
                disabled
              >
                Register
              </Button>
            )}
          </Row>
        </Form>
      </Col>
    </Container>
  );
}
