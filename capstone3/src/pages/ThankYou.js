import React from "react";
import { Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

function ThankYou() {
  return (
    <Container style={{ height: "50vh" }}>
      <Row className="text-center mt-5 mb-5">
        <h1>Thank You!</h1>

        <p>
          Thank you for your purchase! Expect delivery to arrive at your
          location 2-3 days after order.
        </p>
        <p>
          <Link to="/products">Continue shopping</Link>
        </p>
      </Row>
    </Container>
  );
}

export default ThankYou;
